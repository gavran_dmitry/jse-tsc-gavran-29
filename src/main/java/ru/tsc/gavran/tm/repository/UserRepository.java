package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IUserRepository;
import ru.tsc.gavran.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return list.stream()
                .filter(u -> u.getEmail().equals(email))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeUserById(@NotNull final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

}
