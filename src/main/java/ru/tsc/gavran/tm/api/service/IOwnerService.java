package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    @NotNull
    E add(@NotNull final String userId, @Nullable final E entity);

    void remove(@NotNull final String userId, @Nullable final E entity);

    @NotNull
    List<E> findAll(@NotNull final String userId);

    void clear(@NotNull final String userId);

    @NotNull
    E findById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    E removeById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    E removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    E findByIndex(@Nullable final String userId, @Nullable final Integer id);

}
